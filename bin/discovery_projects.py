#!/usr/bin/python

import subprocess
import sys
import json

cmd = subprocess.Popen('/usr/bin/prstat -J -c 5 1', shell=True, stdout=subprocess.PIPE)
start = False
data = []
for line in cmd.stdout:
   line = line.strip('\n')
   if start and "pkg" in line:
      pkgname = line.split()[-1]
      command = "/usr/bin/projects -l " + pkgname + "|egrep comment|cut -d: -f2|xargs"
      desc = subprocess.check_output(command, shell=True).strip()
      data.append({"{#PKGNAME}":pkgname, "{#PKGDESC}":desc})
   if "PROJID" in line:
      start = True

print '{ "data": ' + json.dumps(data) + ' }'
