#!/usr/bin/python

import subprocess
import os
import sys
import time
import argparse
import json
from os import listdir
from threading import Timer


def printExit():
  print(0)
  sys.exit()

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='NFS Test', epilog='Usage')
  parser.add_argument("--mountpoint",   action="store")
  args = parser.parse_args()
  if args.mountpoint:   
    timer = Timer(10, printExit, [])
    timer.start()
    try:
      ret = listdir(args.mountpoint)
      print(1)
    except OSError as ex:
      printExit() 
    finally:
      timer.cancel()    
  else:
    printExit()
