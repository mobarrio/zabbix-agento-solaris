#!/usr/bin/python

import subprocess
import sys
import json

cmd = subprocess.Popen('/usr/bin/prstat -J -c 5 1', shell=True, stdout=subprocess.PIPE)
start = False
data = []
pkgname = sys.argv[1]
cpu_usage = 0
for line in cmd.stdout:
   line = line.strip('\n')
   if start and pkgname in line:
      cpu_usage = line.split()[6].strip('%')
   if "PROJID" in line:
      start = True
print cpu_usage
