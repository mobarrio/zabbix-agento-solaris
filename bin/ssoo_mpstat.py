#!/usr/bin/env python

# --------------------------------
# Author: Mariano J. Obarrio Miles
# Mail: <mariano.obarrio@gmail.com>
# License: GPLv3
# --------------------------------
import os
import argparse
import optparse
import json
import logging
import select
import subprocess
import re

from os import popen, mkdir

CMD = "/usr/bin/mpstat 1 2 | /usr/bin/awk 'BEGIN {i=0} { if($1 == 'CPU') i++; if(i>1)print}'"
if not os.path.exists("/usr/bin/mpstat"):
   print("Command client is not exist on " + CMD)
   exit(0);

discovery = {}

#
# Funcion que recolecta los datos via SP
#
def datagather(discover):
   if args.debug:       print "- Debug       : ACTIVADO"
   procs = 0
   mpstat = {}
   discovery = {}
   mpstat["cpus"] = {}
   discovery["data"] = []

   with popen(CMD) as rawdata:
      data = rawdata.read()
      for line in data.splitlines():
         procs = procs + 1
         line  = line.strip()
         pattern = re.compile(r'\s+')
         line = re.sub(pattern, ',', line)
         cpu,minf,mjf,xcal,intr,ithr,csw,icsw,migr,smtx,srw,syscl,usr,sys,st,idl = line.split(",")
         mpstat["cpus"][cpu] = {}
         mpstat["cpus"][cpu]["minf"] = minf
         mpstat["cpus"][cpu]["mjf"] = mjf
         mpstat["cpus"][cpu]["xcal"] = xcal
         mpstat["cpus"][cpu]["intr"] = intr
         mpstat["cpus"][cpu]["ithr"] = ithr
         mpstat["cpus"][cpu]["csw"] = csw
         mpstat["cpus"][cpu]["icsw"] = icsw
         mpstat["cpus"][cpu]["migr"] = migr
         mpstat["cpus"][cpu]["smtx"] = smtx
         mpstat["cpus"][cpu]["srw"] = srw
         mpstat["cpus"][cpu]["syscl"] = syscl
         mpstat["cpus"][cpu]["usr"] = 100-int(usr)
         mpstat["cpus"][cpu]["sys"] = 100-int(sys)
         mpstat["cpus"][cpu]["st"] = 100-int(st)
         mpstat["cpus"][cpu]["idl"] = 100-int(idl)
         disc = {}
         disc["{#CPUID}"] = cpu
         discovery["data"].append(disc)

   mpstat["nproc"] = procs
   if discover:
      return(json.dumps(discovery))
   else:
      return(json.dumps(mpstat))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Solaris threads core utilization', epilog='Usage')
    parser.add_argument("--gather",      action="store_true")
    parser.add_argument("--discovery",   action="store_true")
    parser.add_argument('--debug',       action='store_true')

    args = parser.parse_args()
    try:
        if   args.gather:    print datagather(False)
        elif args.discovery: print datagather(True)
        else:                print "ERROR"
    except ValueError, e:
        print e



