#!/usr/bin/python

import subprocess
import sys
import json

cmd = subprocess.Popen('/usr/bin/ps -ef|/usr/bin/egrep -i _pmon|/usr/bin/egrep -v grep', shell=True, stdout=subprocess.PIPE)
data = []
for line in cmd.stdout:
   line = line.strip('\n')
   pmon = line.split()[-1].split("_")[-1]
   data.append({"{#PMON}":pmon})

print '{ "data": ' + json.dumps(data) + ' }'
