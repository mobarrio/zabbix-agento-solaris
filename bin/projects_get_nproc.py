#!/usr/bin/python

import subprocess
import sys
import json

cmd = subprocess.Popen('/usr/bin/prstat -J -c 5 1', shell=True, stdout=subprocess.PIPE)
start = False
data = []
pkgname = sys.argv[1]
usage = 0
for line in cmd.stdout:
   line = line.strip('\n')
   if start and pkgname in line:
      usage = line.split()[1]
   if "PROJID" in line:
      start = True
print usage
