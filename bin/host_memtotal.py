#!/usr/bin/python

import subprocess
import sys
import json


command = "/usr/sbin/prtconf -v|/usr/bin/egrep 'Memory size'|/usr/bin/nawk '{print $(NF-1)}'"
print subprocess.check_output(command, shell=True).strip()
