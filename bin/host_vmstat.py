#!/usr/bin/python

import subprocess
import sys
import json
import argparse

parser = argparse.ArgumentParser(description='Process vmstat', epilog='Usage')
parser.add_argument("--kthr_runq",       action="store_true")
parser.add_argument("--kthr_blocked",    action="store_true")
parser.add_argument("--kthr_waiting",    action="store_true")
parser.add_argument("--swapfree",   action="store_true")
parser.add_argument("--memfree",    action="store_true")
parser.add_argument("--pagein",     action="store_true")
parser.add_argument("--pageout",    action="store_true")
parser.add_argument("--scanrate",   action="store_true")
args = parser.parse_args()

try:
   if   args.kthr_runq:    pos = 0
   elif args.kthr_blocked: pos = 1
   elif args.kthr_waiting: pos = 2
   elif args.swapfree:     pos = 3
   elif args.memfree:      pos = 4
   elif args.pagein:       pos = 7
   elif args.pageout:      pos = 8
   elif args.scanrate:     pos = 11
   else:                   pos = -1
   if pos < 0:
      print -1
   else:
      cmd = subprocess.Popen('/usr/bin/vmstat 1 2|/usr/bin/tail -1', shell=True, stdout=subprocess.PIPE)
      for line in cmd.stdout:
         line = line.strip('\n')
         if line:
            vmstat = line.split()
            print format(vmstat[pos])
except ValueError, e:
   print e

